package main

import (
	"fmt"
	"gitlab.com/derbes/modules"
)

func main() {
	fmt.Println("Hello")

	string1 := "qwert"
	string2 := "abcde"
	str1, str2 := modules.Conc(string1,string2)
	fmt.Println(str1, str2)

	a:= 2
	b:= -2
	c:= 0
	x1, x2 := modules.SquareRoot(a,b,c)
	fmt.Println(x1,x2)

	num:= 5
	left:= modules.ShiftLeft(num)
	right:= modules.ShiftRight(num)
	fmt.Println(left, right)
}